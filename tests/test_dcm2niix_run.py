"""Testing for functions within dcm2niix_run.py script."""

from pathlib import Path

import pytest
from flywheel_gear_toolkit import GearToolkitContext

import run
from fw_gear_dcm2niix.dcm2niix import dcm2niix_run
from run import resolve_and_tag

ASSETS_DIR = Path(__file__).parent / "assets"


@pytest.fixture
def dcm2niix_run_mocked(mocker):
    def _inner(returncode, stdout):
        mock_converter = mocker.patch(
            "fw_gear_dcm2niix.dcm2niix.dcm2niix_run.Dcm2niixEnhanced"
        )

        mock_converter.return_value.run.return_value.runtime.returncode = returncode
        mock_converter.return_value.run.return_value.runtime.stdout = stdout

        return mock_converter

    return _inner


def test_ConvertDirectory_Success(tmpdir, dcm2niix_run_mocked):
    """Assertion to check ConvertDirectory runs and calls
    Dcm2niixEnhanced.run().
    """

    mock_converter = dcm2niix_run_mocked(0, "test")

    output = dcm2niix_run.convert_directory(
        source_dir=Path(tmpdir.mkdir("source_dir")),
        output_dir=Path(tmpdir.mkdir("output_dir")),
        comment="test",
        compress_images=3,
        convert_only_series="1 12 23",
        lossless_scaling="y",
    )

    converter = mock_converter.return_value

    assert output
    assert converter.inputs.source_dir == Path(f"{tmpdir}/source_dir")
    assert converter.inputs.output_dir == Path(f"{tmpdir}/output_dir")
    assert converter.inputs.bids_format
    assert converter.inputs.anon_bids
    assert converter.inputs.comment == "test"
    assert converter.inputs.compress == "3"
    assert converter.inputs.compression == 6
    assert converter.inputs.series_numbers == "1 12 23"
    assert not converter.inputs.crop
    assert converter.inputs.out_filename == "%p_%s"
    assert not converter.inputs.ignore_deriv
    assert converter.inputs.args == "-l y"
    assert converter.inputs.merge_imgs == 2
    assert not converter.inputs.to_nrrd
    assert converter.inputs.philips_float
    assert not converter.inputs.single_file
    assert not converter.inputs.has_private
    assert not converter.inputs.verbose


def test_ConvertDirectory_CompressionLevel_CatchError(tmpdir):
    """Assertion to check that convert_directory() throws error
    and exits when compression_level is invalid.
    """

    with pytest.raises(SystemExit):
        assert dcm2niix_run.convert_directory(
            source_dir=Path(tmpdir.mkdir("source_dir")),
            output_dir=Path(tmpdir.mkdir("output_dir")),
            compression_level=11,
        )


def test_ConvertDirectory_Fail(tmpdir, dcm2niix_run_mocked):
    """Assertion to check ConvertDirectory throws error and exits
    if dcm2niix conversion fails.
    """

    _ = dcm2niix_run_mocked(1, "test")

    with pytest.raises(SystemExit):
        assert dcm2niix_run.convert_directory(
            source_dir=Path(tmpdir.mkdir("source_dir")),
            output_dir=Path(tmpdir.mkdir("output_dir")),
        )


def test_ConvertDirectory_IgnoreErrors(tmpdir, dcm2niix_run_mocked):
    """Assertion to check ConvertDirectory does not exit on exception
    when ignore_errors is selected.
    """

    mock_converter = dcm2niix_run_mocked(1, "test")

    output = dcm2niix_run.convert_directory(
        source_dir=Path(tmpdir.mkdir("source_dir")),
        output_dir=Path(tmpdir.mkdir("output_dir")),
        ignore_errors=True,
    )

    assert output
    assert mock_converter.called


CONFIG_JSON = {
    "destination": {"id": "66a119e956f95efba0c5b5ff", "type": "acquisition"},
    "inputs": {
        "dcm2niix_input": {
            "hierarchy": {"id": "66a119e956f95efba0c5b5ff", "type": "acquisition"},
            "object": {
                "type": "dicom",
                "mimetype": "application/zip",
                "modality": "MR",
                "classification": {
                    "Intent": [],
                    "Features": ["3D"],
                    "Measurement": ["T2"],
                },
                "tags": ["file-classifier", "file-metadata-importer"],
                "info": {
                    "header": {
                        "dicom": {
                            "AcquisitionDate": "20200122",
                            "WindowWidth": 6,
                            "dBdt": 0,
                        }
                    }
                },
            },
            "location": {
                "path": "/flywheel/v0/input/dcm2niix_input/10 - anat-T2w.dicom.zip",
                "name": "10 - anat-T2w.dicom.zip",
            },
            "base": "file",
        }
    },
    "config": {
        "anonymize_bids": True,
        "bids_sidecar": "y",
        "coil_combine": False,
        "comment": "",
        "compress_images": "y",
        "compression_level": 6,
        "convert_only_series": "all",
        "crop": False,
        "dcm2niix_verbose": False,
        "debug": False,
        "decompress_dicoms": False,
        "filename": "%f_%p_%t_%s",
        "ignore_derived": False,
        "ignore_errors": False,
        "lossless_scaling": "o",
        "merge2d": 2,
        "output_nrrd": False,
        "philips_scaling": True,
        "remove_incomplete_volumes": False,
        "sanitize_filename": True,
        "save_sidecar_as_metadata": False,
        "single_file_mode": False,
        "tag": "dcm2niix",
        "tag_on_failure": False,
        "text_notes_private": False,
    },
    "job": None,
}


@pytest.fixture
def setup_mocked(mocker):
    mock_setup = mocker.patch("fw_gear_dcm2niix.utils.resolve.setup")
    mock_setup.return_value = {
        "acquisition": {
            "files": [
                {
                    "name": "10_anat_T2w_anat-T2w_20200105142947_10.json",
                    "type": "source code",
                    "classification": {
                        "Intent": [],
                        "Features": ["3D"],
                        "Measurement": ["T2"],
                    },
                    "info": {
                        "header": {
                            "dicom": {
                                "FileMetaInformationGroupLength": 194,
                                "ImplementationClassUID": "1.3.12.2.1107.5.2.0.99999.9",
                                "WindowWidth": 8.0,
                                "dBdt": 0.0,
                            }
                        }
                    },
                    "modality": "MR",
                },
                {
                    "name": "10_anat_T2w_anat-T2w_20200105142947_10.nii.gz",
                    "type": "nifti",
                    "classification": {
                        "Intent": [],
                        "Features": ["3D"],
                        "Measurement": ["T2"],
                    },
                    "info": {
                        "header": {
                            "dicom": {
                                "FileMetaInformationGroupLength": 194,
                                "ImplementationClassUID": "1.3.12.2.1107.5.2.0.99999.9",
                                "WindowWidth": 8.0,
                                "dBdt": 0.0,
                            }
                        }
                    },
                    "modality": "MR",
                },
            ]
        }
    }
    return mock_setup


@pytest.fixture
def generate_resolve_args_mocked(mocker):
    mock_resolve_args = mocker.patch(
        "fw_gear_dcm2niix.utils.parse_config.generate_resolve_args"
    )
    mock_resolve_args.return_value = {
        "tag": "dcm2niix",
        "retain_sidecar": False,
        "save_sidecar_as_metadata": False,
    }
    return mock_resolve_args


def mock_update_file(first_filename, tags):
    pass


class MockFile:
    def __init__(self, tags):
        self.tags = tags


def mock_get_file(input_filename, gear_context, whatever):
    return MockFile(tags=["tag1", "tag2", "tag3"])


def test_resolve_and_tag_works(
    monkeypatch, setup_mocked, generate_resolve_args_mocked, caplog
):
    """Assertion to check that resolve_and_tag() works."""

    with GearToolkitContext(input_args=list()) as gear_context:
        monkeypatch.setattr(gear_context.metadata, "update_file", mock_update_file)
        monkeypatch.setattr(
            run.flywheel_gear_toolkit.utils.metadata, "get_file", mock_get_file
        )

        gear_context.config_json = CONFIG_JSON

        output_image_files = [
            "/flywheel/v0/work/10_anat_T2w_anat-T2w_20200105142947_10.nii.gz"
        ]
        output_sidecar_files = [
            "/flywheel/v0/work/10_a1Gnat_T2w_anat-T2w_20200105142947_10.json"
        ]
        dcm2niix_input_dir = "/flywheel/v0/work/10_anat_T2w"

        resolve_and_tag(
            gear_context, output_image_files, output_sidecar_files, dcm2niix_input_dir
        )

        assert setup_mocked.called
        assert generate_resolve_args_mocked.called
        for record in caplog.records:
            assert (
                "No BIDS sidecar file or sidecar metadata will be generated."
                in record.message
            )
            assert record.levelname == "WARNING"
