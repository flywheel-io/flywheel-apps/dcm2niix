"""Testing for functions within fw_gear_dcm2niix.utils.parse_config.py script."""

from pathlib import Path
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dcm2niix.utils.parse_config import (
    generate_dcm2niix_args,
    generate_prep_args,
    generate_resolve_args,
)

ASSETS_DIR = Path(__file__).parent / "assets"

dummy_config = {
    "comment": "Eat more SPAM",
    "anonymize_bids": False,
    "compress_images": False,
    "compression_level": "very high",
    "convert_only_series": False,
    "crop": False,
    "filename": "%f",
    "ignore_derived": True,
    "ignore_errors": True,
    "lossless_scaling": 1,
    "merge2d": True,
    "output_nrrd": "maybe",
    "philips_scaling": 1,
    "single_file_mode": True,
    "text_notes_private": True,
    "dcm2niix_verbose": False,
    "remove_incomplete_volumes": False,
    "decompress_dicoms": False,
    "sanitize_filename": False,
    "bids_sidecar": "y",
    "save_sidecar_as_metadata": "n",
}

dummy_config_json = {
    "inputs": {
        "dcm2niix_input": {"object": {"classification": "test", "modality": "test"}}
    }
}


def test_GenerateDcm2niixArgs_DicomFilename(tmpdir):
    """Unit tests for `generate_dcm2niix_args` when filename is %dicom%

    The "filename" it returns should be the same as the DICOM filename
    """

    # Note: move to a clean folder, in case the call to GearToolkitContext picks
    # the manifest or configuration.
    tmpdir.chdir()

    dicom_filestem = "bar"
    dicom_filepath = f"/foo/{dicom_filestem}.dcm.zip"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config["filename"] = "%dicom%"
    gear_context.config_json = dummy_config_json
    gear_context.get_input_path.return_value = dicom_filepath

    gear_args = generate_dcm2niix_args(gear_context)

    assert gear_args["filename"] == dicom_filestem


def test_GenerateDcm2niixArgs_NotDicomFilename(tmpdir):
    """Unit tests for `generate_gear_args` when filename is NOT %dicom%

    The "filename" it returns should be the same as it was passed in
    """

    # Note: move to a clean folder, in case the call to GearToolkitContext picks
    # the manifest or configuration.
    tmpdir.chdir()

    not_exactly_dicom_filename = "not_exactly_%dicom%"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config["filename"] = not_exactly_dicom_filename
    gear_context.config_json = dummy_config_json
    gear_context.get_input_path.return_value = "/foo/bar.dcm.zip"

    gear_args = generate_dcm2niix_args(gear_context)

    assert gear_args["filename"] == not_exactly_dicom_filename


def test_GenerateDcm2niixArgs_LongComment_CatchError(tmpdir):
    """Assertion to check error handling for len(comment) > 24."""

    # Note: move to a clean folder, in case the call to GearToolkitContext picks
    # the manifest or configuration.
    tmpdir.chdir()

    dicom_filestem = "bar"
    dicom_filepath = f"/foo/{dicom_filestem}.dcm.zip"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config["comment"] = "This is much too long by far!"
    gear_context.config_json = dummy_config_json
    gear_context.get_input_path.return_value = dicom_filepath

    with pytest.raises(SystemExit):
        assert generate_dcm2niix_args(gear_context)


def test_GeneratePrepArgs_BadFilepath(tmpdir):
    """Assertion to check error handling for if input file doesn't exist."""

    # Note: move to a clean folder, in case the call to GearToolkitContext picks
    # the manifest or configuration.
    tmpdir.chdir()

    bad_filepath = "/this/file_does_not_exist.dcm.zip"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config_json = dummy_config_json
    gear_context.get_input_path.return_value = bad_filepath

    with pytest.raises(SystemExit):
        assert generate_prep_args(gear_context)


def test_GeneratePrepArgs_RecFile():
    """Assertion to check solo parrec handling."""

    par_file = f"{ASSETS_DIR}/parrec_solo.PAR"
    rec_file = f"{ASSETS_DIR}/parrec_solo.REC"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config_json = dummy_config_json
    gear_context.get_input_path.side_effect = (par_file, rec_file, rec_file)

    output = generate_prep_args(gear_context)

    assert output
    assert (output["infile"]) == par_file
    assert (output["rec_infile"]) == rec_file


def test_GenerateResolveArgs_Success():
    """Assertion to test the successful resolution of generate_gear_args."""
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.config_json = dummy_config_json

    output = generate_resolve_args(gear_context)

    assert output["ignore_errors"]
    assert output["retain_sidecar"]
    assert not (output["retain_nifti"])
    assert (output["output_nrrd"]) == "maybe"
    assert (output["classification"]) == "test"
    assert (output["modality"]) == "test"
    assert not (output["save_sidecar_as_metadata"])
