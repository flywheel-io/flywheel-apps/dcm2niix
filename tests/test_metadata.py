"""Testing for functions within metadata.py script."""

from contextlib import nullcontext as does_not_raise
from pathlib import Path

import pytest
from flywheel_gear_toolkit.testing.files import create_dcm
from fw_gear_file_metadata_importer.files.dicom import get_dicom_header

from fw_gear_dcm2niix.utils import metadata

ASSETS_DIR = Path(__file__).parent / "assets"


@pytest.fixture
def capture_mocked(mocker):
    mock_capture = mocker.patch("fw_gear_dcm2niix.utils.metadata.capture")

    mock_capture.return_value = {"test_key": "test_val"}

    return mock_capture


def test_Generate_Success(capture_mocked, tmpdir):
    """Assertion to test generate() with dummy input and mocked calls."""

    metadata_to_save = metadata.generate(
        output_image_files=[],
        output_sidecar_files=[],
    )

    assert capture_mocked.called
    assert metadata_to_save == {"test_key": "test_val"}


@pytest.mark.parametrize(
    "test_input,expectation",
    [
        ({"retain_nifti": True, "output_nrrd": False}, does_not_raise()),
        ({"retain_nifti": False, "output_nrrd": True}, does_not_raise()),
        ({"retain_nifti": True, "output_nrrd": True}, pytest.raises(SystemExit)),
        ({"retain_nifti": False, "output_nrrd": False}, pytest.raises(SystemExit)),
    ],
)
def test_Capture_NiftiNrrdLogic_CatchError(test_input, expectation):
    """Assertion to test whether capture() catches
    NIfTI/NRRD logic errors in the configuration.
    """

    with expectation:
        assert metadata.capture(
            output_image_files=[],
            output_sidecar_files=[],
            retain_sidecar=False,
            retain_nifti=test_input["retain_nifti"],
            output_nrrd=test_input["output_nrrd"],
        )


def test_Capture_Succes():
    """Utilizing dicom_single test files, assertion to test
    successful completion of capture().
    """

    dicom_single_nifti = f"{ASSETS_DIR}/valid_output/dicom_single_test_output.nii.gz"
    dicom_single_json = f"{ASSETS_DIR}/valid_output/dicom_single_test_output.json"

    output = metadata.capture(
        output_image_files=[dicom_single_nifti],
        output_sidecar_files=[str(dicom_single_json)],
        dcm2niix_input_dir=(f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/"),
    )

    assert output


def test_DicomMetadataExtraction_AllExcept_Success():
    """Assertion to test basic dicom header that does not
    contain any of the keys searched for in DicomMetadataExtraction.
    """

    dcm = create_dcm()
    header = get_dicom_header(dcm)
    output = metadata.dicom_metadata_extraction(header)

    for key in output:
        assert not output[key]


def test_CreateFileMetadata_NoSidecar_Success(tmpdir):
    """Assertion to test dict created by CreateFileMetadata."""

    test_input = {
        "name": f"{tmpdir}/test_file.txt",
        "type": "txt",
        "classification": "classification info goes here",
        "metadata": {"dicom_data": "dicom header goes here"},
        "modality": "modality info goes here",
    }
    expected_output = test_input.copy()
    expected_output["name"] = "test_file.txt"
    expected_output["info"] = {"header": {"dicom": "dicom header goes here"}}
    del expected_output["metadata"]

    actual_output = metadata.create_file_metadata(
        filename=test_input["name"],
        filetype=test_input["type"],
        classification=test_input["classification"],
        metadata=test_input["metadata"],
        modality=test_input["modality"],
        retain_sidecar=True,
    )

    assert actual_output == expected_output


def test_CreateFileMetadata_WithSidecar_Success(tmpdir):
    """Assertion to test dict created by CreateFileMetadata for past BIDS workflow."""

    test_input = {
        "name": f"{tmpdir}/test_file.txt",
        "type": "txt",
        "classification": "classification info goes here",
        "metadata": {
            "dicom_data": {"a dicom": "with some tags"},
            "sidecar_info": {"a sidecar": "with more info"},
        },
        "modality": "modality info goes here",
    }
    expected_output = test_input.copy()
    expected_output["name"] = "test_file.txt"
    expected_output["info"] = test_input["metadata"]["sidecar_info"]
    del expected_output["metadata"]

    actual_output = metadata.create_file_metadata(
        filename=test_input["name"],
        filetype=test_input["type"],
        classification=test_input["classification"],
        metadata=test_input["metadata"],
        modality=test_input["modality"],
        retain_sidecar=False,
    )

    assert actual_output == expected_output


def test_CreateFileMetadata_WithSidecar_JSON(tmpdir):
    """Assertion to test dict is None by CreateFileMetadata for past BIDS workflow."""

    test_input = {
        "name": f"{tmpdir}/test_file.json",
        "type": "source code",
        "classification": "classification info goes here",
        "metadata": {
            "dicom_data": {"a dicom": "with some tags"},
            "sidecar_info": {"a sidecar": "with more info"},
        },
        "modality": "modality info goes here",
    }
    expected_output = None

    actual_output = metadata.create_file_metadata(
        filename=test_input["name"],
        filetype=test_input["type"],
        classification=test_input["classification"],
        metadata=test_input["metadata"],
        modality=test_input["modality"],
        retain_sidecar=False,
    )

    assert actual_output == expected_output


def test_create_file_metadata_does_not_create_dicom_header_for_non_dicom_files(tmpdir):
    test_input = {
        "name": f"{tmpdir}/test_file.txt",
        "type": "txt",
        "classification": "classification info goes here",
        "metadata": {"dicom_data": ""},
        "modality": "MR",
    }

    actual_output = metadata.create_file_metadata(
        filename=test_input["name"],
        filetype=test_input["type"],
        classification=test_input["classification"],
        metadata=test_input["metadata"],
        modality=test_input["modality"],
        retain_sidecar=True,
    )

    assert "info" not in actual_output


def test_RenameInfile_InvalidCharacters_Success(tmpdir):
    """Assertions to check that rename_infile catches, sanitizes,
    and renames file. Function creates a test file with an
    invalid character.
    """

    Path(f"{tmpdir}/test_file_*.txt").touch()
    metadata.rename_infile(Path(f"{tmpdir}/test_file_*.txt"))

    assert Path(f"{tmpdir}/test_file_star.txt").exists()
    assert not Path(f"{tmpdir}/test_file_*.txt").exists()
