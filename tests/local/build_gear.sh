#!/usr/bin/env bash
# Development tag
TAG=1.2.1_1.0.20201102_dev2
LOCAL='no'

SCRIPT_PATH=$(dirname "$0")
GEAR_DIR=$(readlink -f "$SCRIPT_PATH/../../")
echo "GEAR_DIR: $GEAR_DIR"

# Make upload directory
UPLOAD_DIR="${GEAR_DIR}/../upload_zone"
mkdir -p "${UPLOAD_DIR}"

# Copy necessary files to upload directory
cp -r Dockerfile manifest.json pyproject.toml poetry.lock README.md run.py fw_gear_dcm2niix "${UPLOAD_DIR}"

cd "${UPLOAD_DIR}" || exit
echo "${PWD}"

# Build the docker image
docker build -t flywheel/dcm2niix:${TAG} ./

# Test gear locally
if [ ${LOCAL} == 'yes' ]; then
	fw gear local --dcm2niix_input "$GEAR_DIR"/tests/assets/aliza_siemens_trio.zip
fi

# Finish 
echo "Finished."
