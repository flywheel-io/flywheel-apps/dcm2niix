FROM flywheel/python:3.12-alpine AS build

# Allows one to pass this in as a build arg, i.e. `docker build --arg=DCMCOMMIT=<my_other_commit>`
ARG DCM2NIIX_TAG="v1.0.20240202"

RUN apk update && apk upgrade && apk add --no-cache \
    # dcm2niix build-deps
    git \
    curl \
    build-base \
    cmake \
    pkgconfig \
    # dcm2niix link-deps
    pigz \
    openjpeg-dev && \
    rm -rf /var/cache/apk/*

RUN curl -L  https://github.com/rordenlab/dcm2niix/archive/refs/tags/$DCM2NIIX_TAG.zip -o dcm2niix.zip && \
    unzip dcm2niix.zip -d /usr/local

# hadolint ignore=DL3003, DL4006
RUN mkdir /usr/local/dcm2niix-${DCM2NIIX_TAG#v}/build && \
    cd /usr/local/dcm2niix-${DCM2NIIX_TAG#v}/build && \
    cmake \
        -DUSE_OPENJPEG=System \
        -DMY_DEBUG_GE=ON \
        -DUSE_GIT_PROTOCOL=OFF ../ && \
    make && \
    # Binary in /usr/local/bin/dcm2niix
    make install
    
FROM flywheel/python:3.12-alpine AS gear
LABEL maintainer="Flywheel <support@flywheel.io>"

# Flywheel spec (v0)
ENV FLYWHEEL="/flywheel/v0"
RUN mkdir -p ${FLYWHEEL}

RUN apk update && apk upgrade && apk add --no-cache \
    curl \
    gcc \
    libc-dev \
    gdcm \
    rm -rf /var/cache/apk/*

# Copy built dcm2niix
COPY --from=build /usr/local/bin/dcm2niix /usr/local/bin/dcm2niix
# Copy in fix_dcm_vols.py from source
WORKDIR ${FLYWHEEL}
ENV FIXDCMCOMMIT=918ee3327174c3c736e7b3839a556e0a709730c8
RUN curl -#L https://raw.githubusercontent.com/VisionandCognition/NHP-Process-MRI/${FIXDCMCOMMIT}/bin/fix_dcm_vols > ${FLYWHEEL}/fix_dcm_vols.py
RUN chmod +x ${FLYWHEEL}/fix_dcm_vols.py

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
#RUN pip install --upgrade pip setuptools wheel
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]